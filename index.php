<?php
    require './GetDataFromSheet.php';

    $sheet_id   = '1Hsu4QPX3Y9JuNHCkPonX-zApPvodJN9d8vZSk_2sTX4';
    $range      = 'A1:E11';
    try {
        //code...
        $sheet = new GetDataFromSheet($sheet_id);
        $data = $sheet->get_data_upto($range)->getValues();
        // echo "<pre> === ";
        // print_r($data);
    } catch (\Throwable $th) {
        echo $th->getMessage();
        die();
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Google Sheet With PHP (TEST)</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>Google Sheet With PHP (TEST)</h2>
  <table class="table table-hover">
    <thead>
      <tr>
      <?php 
            foreach($data[0] as $head)
            {
                echo "<th>".$head."</th>";
            }
        ?>
      </tr>
    </thead>
    <tbody>
        <?php 
            foreach($data as $key => $values)
            {
                if($key >= 1)
                {
                    echo "<tr>";
                    foreach($values as $row)
                    {
                        echo "<td>".$row."</td>";
                    }
                    echo "</tr>";
                }

            }
        ?>
      
    </tbody>
  </table>
  <a href="./importCSV.php" class="btn btn-primary">Import Data</a>
</div>

</body>
</html>
