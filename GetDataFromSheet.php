<?php

require './vendor/autoload.php';

class GetDataFromSheet{

    protected $client;
    public $name = 'Nirmaljeet Singh Task Demo';
    public $api_key_path = 'keys.json';
    public $sheet_id;

    public function __construct($sheet_id)
    {
        $this->client = new Google_Client();
        $this->client->setApplicationName($this->name);
        $this->client->setScopes(Google_Service_Sheets::SPREADSHEETS_READONLY);
        $this->client->setAuthConfig($this->api_key_path);
        $this->client->setAccessType('offline');
        $this->client->setPrompt('select_account consent');
        $this->sheet_id = $sheet_id;
    }

    public function get_data_upto($range) {
        $service    = new Google_Service_Sheets($this->client);
        return $service->spreadsheets_values->get($this->sheet_id, $range);
    }
}