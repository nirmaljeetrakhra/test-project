<?php
    require './GetDataFromSheet.php';

    $sheet_id   = '1Hsu4QPX3Y9JuNHCkPonX-zApPvodJN9d8vZSk_2sTX4';
    $range      = 'A1:E11';
    try {
        $sheet = new GetDataFromSheet($sheet_id);
        $data = $sheet->get_data_upto($range)->getValues();
        $headers = array(
            "Content-type"        => "application/text",
            "Content-Disposition" => "attachment; filename=GoogleSheet.csv",
            "Pragma"              => "no-cache",
            "Cache-Control"       => "must-revalidate, post-check=0, pre-check=0",
            "Expires"             => "0"
        );
        
        $columns = [];
        foreach($data[0] as $head)
        {
            $columns[] = $head;
        }
        $file = fopen('php://output', 'w');
        fputcsv($file, $columns);
        foreach($data as $key => $values)
        {
            if($key >= 1)
            {
                $rows = [];
                foreach($values as $row)
                {
                    $rows[] = $row;
                }
                fputcsv($file,$rows);
            }
        }
        fclose($file);
        header('Content-Type: text/csv; application/download');
        header('Content-Disposition: attachment; filename="GoogleSheet.csv"');
        header("Content-Length: " . filesize($file));
        $fp = fopen($file, "r");
        fpassthru($fp);
        fclose($fp);
        return $fp;
    } catch (\Throwable $th) {
        echo $th->getMessage();
        die();
    }
?>